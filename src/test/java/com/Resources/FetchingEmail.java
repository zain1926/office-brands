package com.Resources;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.TEAF.framework.Utilities;
import com.google.common.io.Files;

public class FetchingEmail {



	public static String fetch() {
		String sub = null;

		try {

			// Create object of Property file
			Properties props = new Properties();
			props.put("mail.smtp.host", "email-smtp.us-west-2.amazonaws.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "587");
			props.put("mail.smtp.starttls.enable", "true");

			// This will handle the complete authentication
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {

				protected PasswordAuthentication getPasswordAuthentication() {

					return new PasswordAuthentication("AKIAJKMT27IM53KGLPLA", "AuhwF8Plo8elSFEjC/WjUWpWrCFYJVMVdPwsHAGLh9uY");

				}

			});
			System.out.println("Authentication succcessfull");
			Session emailSession = Session.getDefaultInstance(props);
			// emailSession.setDebug(true);

			// create the POP3 store object and connect with the pop server
			Store store = emailSession.getStore("smtps");

			store.connect("email-smtp.us-west-2.amazonaws.com", "AKIAJKMT27IM53KGLPLA", "AuhwF8Plo8elSFEjC/WjUWpWrCFYJVMVdPwsHAGLh9uY");

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			System.out.println("messages.length---" + messages.length);
			for (int i = messages.length; i >= messages.length - 15; i--) {
				// System.out.println();

				Message message = messages[i - 1];
				if (message.getSubject().endsWith("Received") && sub == null) {
					sub = message.getSubject().toString();
					// System.out.println("---------------------------------");

					writePart(message);

				}
				/*
				 * String line = reader.readLine();
				 * 
				 * if ("YES".equals(line)) { message.writeTo(System.out); } else if
				 * ("QUIT".equals(line)) { break; }
				 */}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// System.out.println(sub);
		return sub;

	}

	/*
	 * This method checks for content-type based on which, it processes and fetches
	 * the content of the message
	 */
	public static void writePart(Part p) throws Exception {
		File f1 = new File(System.getProperty("user.dir") + "\\output\\order-content.xml");
		File f2 = new File(System.getProperty("user.dir") + "\\output\\OrderConfirmation.txt");
		String contentBody = null;
		if (p instanceof Message)
			// Call methos writeEnvelope
			contentBody = writeEnvelope((Message) p);

		/*
		 * System.out.println("----------------------------");
		 * System.out.println("CONTENT-TYPE: " + p.getContentType());
		 */
		// check if the content is plain text
		if (p.isMimeType("text/plain")) {
			/*
			 * System.out.println("This is plain text");
			 * System.out.println("---------------------------");
			 */
			System.out.println((String) p.getContent());
		}
		// check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			/*
			 * System.out.println("This is a Multipart");
			 * System.out.println("---------------------------");
			 */
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < 2; i++) {
				if (i == 1) {
					BodyPart bodyPart = mp.getBodyPart(i);
					bodyPart.writeTo(new FileOutputStream(f1));

				} else {
					BodyPart bodyPart = mp.getBodyPart(i);
					Object content = bodyPart.getContent();
					String string = content.toString();

					org.jsoup.nodes.Document doc = Jsoup.parse(string);
					String o = doc.text();

					String wholeBody = contentBody + "\n" + o;
					FileWriter x = new FileWriter(f2);
					x.write(wholeBody);
					x.close();

				}
			}
		}
		/*
		 * //check if the content is a nested message else if
		 * (p.isMimeType("message/rfc822")) {
		 * System.out.println("This is a Nested Message");
		 * System.out.println("---------------------------"); writePart((Part)
		 * p.getContent()); }
		 */
		// check if the content is an inline image
		// else if (p.isMimeType("image/jpeg")) {
		// System.out.println("--------> image/jpeg");
		// Object o = p.getContent();
		//
		// InputStream x = (InputStream) o;
		// // Construct the required byte array
		// System.out.println("x.length = " + x.available());
		// while ((i = (int) ((InputStream) x).available()) > 0) {
		// int result = (int) (((InputStream) x).read(bArray));
		// if (result == -1)
		// int i = 0;
		// byte[] bArray = new byte[x.available()];
		//
		// break;
		// }
		// FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
		// f2.write(bArray);
		// }
		else if (p.getContentType().contains("image/")) {
			System.out.println("content type" + p.getContentType());
			File f = new File("image" + new Date().getTime() + ".jpg");
			DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
			com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
			byte[] buffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = test.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				/*
				 * System.out.println("This is a string");
				 * System.out.println("---------------------------");
				 */
				System.out.println((String) o);
			} else if (o instanceof InputStream) {
				/*
				 * System.out.println("This is just an input stream");
				 * System.out.println("---------------------------");
				 */
				InputStream is = (InputStream) o;
				is = (InputStream) o;
				int c;
				while ((c = is.read()) != -1) {
					System.out.write(c);
				}
				// is.close();

			}

			else {
				System.out.println("This is an unknown type");
				System.out.println("---------------------------");
				System.out.println(o.toString());
			}
		}

	}

	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	public static String writeEnvelope(Message m) throws Exception {
		/*
		 * System.out.println("This is the message envelope");
		 * System.out.println("---------------------------");
		 */
		Address[] a;
		String From = null, To = null, subject = null;
		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++) {
				From = a[j].toString();
			}
		}

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++) {
				To = a[j].toString();
			}
		}

		// SUBJECT
		if (m.getSubject() != null) {
			subject = m.getSubject();

		}
		return "From :" + From + "\n " + "To :" + To + "\n" + "Subject " + subject;

	}

	public static void readXml(String arg) {

		try {

			List<String> x = new ArrayList();
			File file1 = new File(System.getProperty("user.dir") + "\\output\\order-content.xml");
			List<String> readLines = FileUtils.readLines(file1);
			for (int i = 6; i < readLines.size(); i++) {
				String string = readLines.get(i);
				x.add(string);
			}
			// System.out.println(x);

			FileWriter writer = new FileWriter(file1);
			for (String str : x) {
				writer.write(str);
			}
			writer.close();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(file1);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			// System.out.println("Root element :" +
			// doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName(arg);
			int length = nList.getLength();
			// System.out.println(length);
			Assert.assertEquals(0, length);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
public static void main(String[] args) throws Throwable {
	DeleteOutput_folder();
//	 fetch();
}
	public static void mailReportsAutomatically(String mailId, String ccMail) throws Throwable {

		// Create object of Property file
		Properties props = new Properties();
		props.put("mail.smtp.host", "email-smtp.us-west-2.amazonaws.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.starttls.enable", "true");

		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication("AKIAJKMT27IM53KGLPLA", "AuhwF8Plo8elSFEjC/WjUWpWrCFYJVMVdPwsHAGLh9uY");

			}

		});
		System.out.println("Authentication succcessfull");
		try {

			// Create object of MimeMessage class
			Message message = new MimeMessage(session);

			// Set the from address
			message.setFrom(new InternetAddress("webmaster@officenational.com.au"));
			// String mailToId2 = "chandan.navara@royalcyber.com";
			// String mailToId3 = "swathin@royalcyber.com";
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailId));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse(mailToId2));
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccMail));
			// Set the recipient
			// address
			String lDate = LocalDate.now().toString();
			String lTime = LocalTime.now().toString();
			// Add the subject link

			String subject = "RC TEAF Execution Report -  Build : " + System.getProperty("BuildNo", "Scheduled Jenkins Execution")
					+ ":" + " on " + lDate + ": " + lTime;
			message.setSubject(subject);

			// Create object to add multimedia type content
			BodyPart messageBodyPart1 = new MimeBodyPart();

			// Set the body of email
			messageBodyPart1.setText("Hi " + "\n" + "PFA Extent Report for " + " Build : "
					+ System.getProperty("BuildNo", "Scheduled Jenkins Execution") + "\n" + "Thanks");

			// Create another object to add another content
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			MimeBodyPart messageBodyPart3 = new MimeBodyPart();

			// Mention the file which you want to send

			Utilities.reportstoZipFile("output", "ExtentReport");
			Utilities.reportstoZipFile("GalenOutput", "UI_Report");

			String fEN = System.getProperty("user.dir") + "/ExtentReport.zip";

			String fUI = System.getProperty("user.dir") + "/UI_Report.zip";
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart1);

			if (java.nio.file.Files.exists(Paths.get(fEN), LinkOption.NOFOLLOW_LINKS)) {
				DataSource source = new FileDataSource(fEN);

				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(source));

				// set the file
				messageBodyPart2.setFileName(fEN);
				multipart.addBodyPart(messageBodyPart2);

			}

			if (java.nio.file.Files.exists(Paths.get(fUI), LinkOption.NOFOLLOW_LINKS)) {
				DataSource source1 = new FileDataSource(fUI);

				// set the handler
				messageBodyPart3.setDataHandler(new DataHandler(source1));

				// set the file
				messageBodyPart3.setFileName(fUI);
				multipart.addBodyPart(messageBodyPart3);

			}

			// Create data source and pass the filename

			// Create object of MimeMultipart class

			// add body part 1

			// add body part 2

			// set the content
			message.setContent(multipart);

			// finally send the email
			Transport.send(message);

			System.out.println("=====Email Sent=====");

		} catch (MessagingException e) {

			throw new RuntimeException(e);

		}
		Utilities.deleteZipFiles("ExtentReport");
		Utilities.deleteZipFiles("UI_Report");

	}
	
	public static void DeleteOutput_folder() {
		
		File f = new File(System.getProperty("user.dir")+"/output/"); 
		f.delete();
	
	}
	
	
}