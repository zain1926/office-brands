@OPDNZ_SmokeTest
Feature: OPD-NZ Home page

@Login_OPDNZ 
Scenario: Login to OPD Store
Given My WebApp 'OfficeBrands' is open
And I should see element 'OPDNZ_Logo' present on page_
When I click 'SignIn_Button'
And I enter 'rctestopdnz' in field 'LogonID'
And I enter 'rctestopdnz' in field 'Password'
And I click 'LogIn'
Then I should see element - 'UserName_Header' contains text 'TESTER'

Scenario: Clearing the product in the Cart
And I click 'OPDNZ_ViewCart'
And I click 'OPDNZ_CurrentOrder'
And I clear the Cart and See Your current order is empty

@CategoryBrowse 
Scenario: Browsing through category and sub-category
And I should see element 'OPDNZ_HeaderRow' present on page_
And I click 'OPDNZ_ShopByProduct'
And I mouse over 'OPDNZ_OfficeSupply'
And I click 'Envelopes'
Then I should see element - 'OPDNZ_PageHeading' contains text 'Envelopes'
And I click 'OPDNZ_Correspondence'
Then I should see element - 'OPDNZ_PageHeading' contains text 'Correspondence'
And I click 'OPDNZ_ColouredEnvelopes'
Then I should see element - 'OPDNZ_PageHeading' contains text 'Coloured Envelopes'
And I should see element 'OPDNZ_Breadcrumb' present on page_
And I get text from 'FirstProductName_pdp' and store
And I mouse over 'FirstProduct_pdpPage'
And I click 'AddToCart'
#Then I should see element 'CartMessage' present on page
And I scroll to top of the page
And I wait for '3' seconds

@SearchProduct 
Scenario: Searching for Product and adding to cart
When I enter 'Copy Paper' in the feild 'Search' using actions
And I click 'Submit_Button'
And I should see element 'SearchResult_Page' present on page_
And I get text from 'FirstProductName_search' and store
And I click 'AddToCart'
#Then I should see element 'CartMessage' present on page
And I click 'FirstProductName_search'
Then I should see text '$$FirstProductName_search' present on page at 'ProductNameHeader_PDP'
And I click 'AddToCart_PDPPage'
#Then I should see element 'CartMessage' present on page

@AddToFavourite  
Scenario: Adding products to favourite
And I click 'Favourite_Tab'
When I enter 'Copy Paper' in the feild 'Search' using actions
And I click 'Submit_Button'
And I should see element 'SearchResult_Page' present on page_
And I click 'AddToFav_Button'
And I click 'RadioButton'
And I enter 'Test RC QA' in field 'FavList_Name'
And I click 'AddToRequisitionList'
Then I should see element - 'ProductAddedPop_FavList' contains text 'Test RC QA'
#And I should see element 'PopUp_FavProductAdded' present on page_
And I click 'ContinueShopping'

@AddFromFavouritePage 
Scenario: Adding products from Favourite Pages
And I click 'Favourite_Tab'
And I should see element - 'Heading_FavouriteList' contains text 'Favorites List'
And I click 'FavListName'
And I enter '2011026' in field 'TextBox_SKU'
And I enter '1' in field 'Quantity_TextBox'
And I click 'Add_Button'
And I should see element - 'SuccessMsg_PopUp' contains text 'The selected item has been added to the requisition list.'
And I clear field 'Fav_Quantity'
And I enter '1' in field 'Fav_Quantity'
And I click 'Actions'
And I click 'AddItemToCurrentOrder'
And I click 'Favourite_Tab'
And I click 'AddedFav_ActionBtn' 
And I click 'AddedFav_DeleteBtn'
And I scroll to top of the page

#And I scroll to 'coordinates' - '0,-1500'

@CartPage 
Scenario: Adding product to cart and submitting order
And I click 'OPDNZ_ViewCart'
And I click 'OPDNZ_CurrentOrder'
Then I should see text '$$FirstProductName_pdp' present on page
And I click 'OPDNZ_Checkout'
And I enter 'Test Order, Please do not ship it.' in field 'AddComment_ShippingMethod'
And I click 'Submit_Order'
And I wait for '5' seconds
Then I should see element 'OrderConfirmationPage' present on page_
And I should see text 'Order Complete' present on page at 'OrderCompletePage_Header'

