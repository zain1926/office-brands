package com.TEAF.stepDefinitions;

import java.awt.image.BufferedImage;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Resources.FetchingEmail;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.TEAF.stepDefinitions.CommonSteps;
import com.aventstack.extentreports.ExtentReports;

import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;

public class CommonSteps_OfficeNational {

	public static ExtentReports extent = new ExtentReports();

	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();

	@When("^I enter random values for '(.*)'$")
	public static void i_enter_randomvalues(String element) throws Exception {
		try {
			String randomAlphabetic = RandomStringUtils.randomAlphabetic(4);
//			System.out.println(randomAlphabetic);
			String value = "RC" + randomAlphabetic;
			WebElement ele = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			CommonSteps.I_wait_for_visibility_of_element(element);
			ele.sendKeys(value);
			HashMapContainer.add("$$" + element, value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	@When("^I select date '(.*)' in the date picker$")
	public static void i_select_the_date_in_the_datepicker(){
		LocalDate now = LocalDate.now();
		String[] split = now.toString().replaceAll("-", "/").split("/", 2);
		String startDate = split[1]+"/"+split[0];
		System.out.println(startDate);
		Date dt = new Date();
		LocalDateTime plusDays = LocalDateTime.from(dt.toInstant()).plusDays(1);
		System.out.println(plusDays);
	}
	
	public static void main(String[] args) {
		i_select_the_date_in_the_datepicker();
	}
	
	@When("^I should see the values '(.*)' in the feild '(.*)'$")
	public static void i_should_see_values_in_the_feild(String expectedText, String location) {
		try {
			if (expectedText.substring(0, 2).equals("$$")) {
				expectedText = HashMapContainer.get(expectedText);
			}
			// System.out.println("Expected"+expectedText);
			wrapFunc.waitForElementPresence(GetPageObjectRead.OR_GetElement(location));
			// WebElement we =
			// driver.findElement(GetPageObjectRead.OR_GetElement(location));
			String actualText = driver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("value");
			WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(location)));
			// System.out.println("actualText"+actualText);

			Assert.assertEquals(expectedText, actualText);
			StepBase.embedScreenshot();
			extent.createTest(location).addScreenCaptureFromPath(Utilities.takeScreenshot(driver));
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@When("^I enter email values for '(.*)'$")
	public static void i_enter_emailvalues(String element) throws Exception {
		try {
			String randomAlphabetic = RandomStringUtils.randomAlphabetic(4);
			System.out.println(randomAlphabetic);
			String value = "RC" + randomAlphabetic + "@royalcyber.com";
			WebElement ele = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			CommonSteps.I_wait_for_visibility_of_element(element);
			ele.sendKeys(value);
			HashMapContainer.add("$$" + element, value);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

//	@When("^I clear the Cart and See Your current order is empty$")
//	public static void clear_the_cart_OfficeBrands() throws Exception {
//		try {
//			 Boolean OrderTotal_CartPage = driver.findElement(GetPageObjectRead.OR_GetElement("OrderTotal")).isDisplayed();
//			if (OrderTotal_CartPage) {
//				JavascriptExecutor js = (JavascriptExecutor) driver;
//				js.executeScript("window.scrollBy(0,5000)");
//				Thread.sleep(3000);
//				driver.findElement(GetPageObjectRead.OR_GetElement("RemoveOrder_button")).click();
//				WebDriverWait wait = new WebDriverWait(driver, 10000);
//				wait.until(ExpectedConditions.alertIsPresent());
//				driver.switchTo().alert().accept();
//			} else {
//				Thread.sleep(3000);
//				System.out.println("There is no product in the cart");
//			}
//
//		} catch (Exception e) {
//			Thread.sleep(3000);
//			System.out.println("There is no product in the cart");
//		}
//
//	}

	@When("^I check the product added in the cart$")
	public static void i_check_the_product_added_cart() {
		try {
			// CommonSteps.I_scroll_to_element("element", "OPDNZ_ViewCart");
			util.scrollToTopOfPage();
			CommonSteps.I_should_see_on_page("OPDNZ_ViewCart");
			CommonSteps.I_click("OPDNZ_ViewCart");
			CommonSteps.I_should_see_text_present_on_page_At("$$ProductName_pdp", "AddedProduct_MiniCart");
			CommonSteps.I_should_see_on_page("AddedProduct_MiniCart");
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			CommonSteps.I_click("OPDNZ_ViewCart");
			CommonSteps.I_should_see_on_page("AddedProduct_MiniCart");
		}
	}

	@When("^I clear the Cart and See Your current order is empty$")
	public static void clear_the_cart_OfficeBrands() throws Exception {
		try {
			String data = driver.findElement(GetPageObjectRead.OR_GetElement("MiniCart_Total")).getText();
			System.out.println(data);

//			boolean displayed = driver.findElement(GetPageObjectRead.OR_GetElement("OPDNZ_OrderTotal")).isDisplayed();
			if (data.equals("0")) {
				System.out.println("There is no product in the cart");
				WebElement emptyOrder = driver.findElement(By.id("WC_EmptyShopCartDisplayf_div_1"));
				String text = emptyOrder.getText();
				Assert.assertTrue(text.contains("Your current order is empty"));
				wrapFunc.highLightElement(emptyOrder);
			} else {
				wrapFunc.scroll("element", "RemoveOrder_button");
				Thread.sleep(2000);
				driver.findElement(GetPageObjectRead.OR_GetElement("RemoveOrder_button")).click();
				WebDriverWait wait = new WebDriverWait(driver, 10000);
				wait.until(ExpectedConditions.alertIsPresent());
				driver.switchTo().alert().accept();
				WebElement emptyOrder = driver.findElement(By.id("WC_EmptyShopCartDisplayf_div_1"));
				String text = emptyOrder.getText();
				Assert.assertTrue(text.contains("Your current order is empty"));
				wrapFunc.highLightElement(emptyOrder);
			}

		} catch (NoSuchElementException e) {
			Thread.sleep(3000);
			System.out.println("There is no product in the cart");
		} catch (Exception e) {

		}

	}

	@When("^I delete the previous existing Fav List$")
	public static void I_Delete_Previous_existing_favList() throws Exception {
		String msg = "You have not created any Favourites Lists yet. To create a new list, click either the Create List button or the Upload List button.";
		try {
			Boolean Fav_List = driver.findElement(GetPageObjectRead.OR_GetElement("NoList_message")).isDisplayed();
			if (driver.findElement(GetPageObjectRead.OR_GetElement("NoList_message")) == null
					|| msg.equals(driver.findElement(GetPageObjectRead.OR_GetElement("NoList_message")).getText())) {
				System.out.println("There is no favourite List");
			} else {
				// System.out.println("else part");
				driver.findElement(GetPageObjectRead.OR_GetElement("Action_Button")).click();
				driver.findElement(GetPageObjectRead.OR_GetElement("DeleteList")).click();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}

	}

	@When("I verify the order number '(.*)' is present in the subject of order confirmation email for provided email address$")
	public static void i_verify_the_subject_of_the_order_placed_email(String ordernumber) throws Exception {
		try {
			String ON = HashMapContainer.get(ordernumber);
			System.out.println(ON);
			String fetch = FetchingEmail.fetch();
			System.out.println(fetch);
			org.junit.Assert.assertTrue(fetch.contains(ON));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@When("I get order number from order complete page and store as '(.*)'$")
	public static void i_get_order_number_(String ordernumber) throws Exception {
		try {
			String s = driver.findElement(By.xpath("//h1[@class='breadcrumb_current']")).getText();
			String[] split = s.split(" ");
			String orderNumber = split[split.length - 1];
			System.out.println(orderNumber);
			HashMapContainer.add("$$" + ordernumber, orderNumber);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("I verify the tagName '(.*)' should not present in XML Attachement")
	public static void i_verify_the_tagName_should_not_prsent(String tag) throws Exception {
		try {
			FetchingEmail.readXml(tag);
			extent.setTestRunnerOutput("<a href='order-content.xml'>Order Content XML</a>");

			extent.setTestRunnerOutput("<a href='OrderConfirmation.txt'>Order confirmation Email</a>");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^I enter username and password to login the application$")
	public static void i_enter_username_password_to_login_the_app() throws Exception {
		try {
			CommonSteps.I_enter_in_field(System.getProperty("UserName", "testuserfep6"), "LogonID");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_enter_in_field(System.getProperty("PassWord", "password"), "Password");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^List of Products under '(.*)'$")
	public static void list_of_products(String arg) {
		if (arg.contains("Contract")) {
			extent.setTestRunnerOutput(
					"<table style=\"border:1 px solid black\"><tr><th>SKU Id</th><th>Product Name</th></tr>"
							+ " <tr> <td>7001110</td> <td>OFFICE NATIONAL A3 ULTRA WHITE CARBON NEUTRAL COPY PAPER 80GSM WHITE PACK 500 SHEETS</td></tr>"
							+ " <tr> <td>7019338</td> <td>INITIATIVE A3 OFFICE COPY PAPER 80GSM WHITE PACK 500 SHEETS</td></tr>    </table>");
		}

		if (arg.contains("List")) {
			extent.setTestRunnerOutput(
					"<table style=\"border:1 px solid black\"><tr><th>SKU Id</th><th>Product Name</th></tr> "
							+ "<tr> <td>7000587</td> <td>INITIATIVE A4 OFFICE COPY PAPER 80GSM WHITE PACK 500 SHEETS</td></tr>"
							+ " <tr> <td>7000264</td> <td>DOUBLE A A4 SMOOTHER COPY PAPER 80GSM WHITE PACK 500 SHEETS</td></tr>  "
							+ " <tr> <td>7006577</td> <td>MONDI COLOR COPY A4 COPY PAPER 90GSM WHITE PACK 500 SHEETS</td></tr>  </table>");
		}

		if (arg.contains("Store Products")) {
			extent.setTestRunnerOutput(
					"<table style=\"border:1 px solid black\"><tr><th>SKU Id</th><th>Product Name</th></tr> "
							+ "<tr> <td>701TEST</td> <td>TEST 1</td></tr>"
							+ " <tr> <td>123456789</td> <td>JADENWINSTON</td></tr>   </table>");
		}
	}

	@When("I get Window Title of next Window$")
	public static void i_get_window_title_Of_Next_Window() throws Exception {
		try {
			String parentWindow = driver.getWindowHandle();
			Set<String> handles = driver.getWindowHandles();
			for (String windowHandle : handles) {
				if (!windowHandle.equals(parentWindow)) {
					driver.switchTo().window(windowHandle);
					String Windowtitle = driver.getTitle();
					System.out.println(Windowtitle);
					driver.close();
					driver.switchTo().window(parentWindow);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@When("^I get the order number from Order Confirmation page and store as '(.*)'$")
	public static void i_get_order_number_from_order_confirmationPage(String on) throws Exception {

		try {
			WebElement oN = driver.findElement(GetPageObjectRead.OR_GetElement("OrderNumber_OrderCompletePage"));
			String text = oN.getText();
			String numberOnly = text.replaceAll("[^0-9]", "");
			HashMapContainer.add(on, numberOnly);
			System.out.println(numberOnly);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}
}